export const MOCK_GROUP_WORKSPACE_DATA = {
  nodes: [
    {
      id: 1,
      name: 'Workspace 1',
      namespace: 'Namespace',
      projectFullPath: 'GitLab.org / GitLab',
      desiredState: 'Running',
      actualState: 'Started',
      displayedState: 'Started',
      url: 'https://127.0.0.1',
      editor: 'VSCode',
      devfile: 'devfile',
      branch: 'master',
      lastUsed: '2020-01-01T00:00:00.000Z',
    },
  ],
};
